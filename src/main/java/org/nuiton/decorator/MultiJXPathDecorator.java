/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Comparator;

/**
 * {@link JXPathDecorator} implementation with multiple contexts.
 *
 * @param <O> type of data to decorate
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @see Decorator
 * @since 2.3
 */
public class MultiJXPathDecorator<O> extends JXPathDecorator<O> {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log =
            LogFactory.getLog(MultiJXPathDecorator.class);

    /** Contexts of the decorator */
    protected Context<O>[] contexts;

    /** context separator */
    protected String separator;

    /** context separator replacement */
    protected String separatorReplacement;

    protected MultiJXPathDecorator(
            Class<O> internalClass,
            String expression,
            String separator,
            String separatorReplacement,
            Context<O>[] contexts) throws IllegalArgumentException,
            NullPointerException {
        super(internalClass, expression, null);
        this.separator = separator;
        this.separatorReplacement = separatorReplacement;
        this.contexts = contexts;

        setContextIndex(0);

        if (log.isDebugEnabled()) {
            log.debug(expression + " --> " + context);
        }
    }

    protected MultiJXPathDecorator(
            Class<O> internalClass,
            String expression,
            String separator,
            String separatorReplacement) throws IllegalArgumentException,
            NullPointerException {
        this(internalClass,
             expression,
             separator,
             separatorReplacement,
             DecoratorUtil.<O>createMultiJXPathContext(expression,
                                                       separator,
                                                       separatorReplacement)
        );
    }

    public void setContextIndex(int index) {
        ensureContextIndex(this, index);
        setContext(contexts[index]);
    }

    public int getNbContext() {
        return contexts.length;
    }

    public String getSeparator() {
        return separator;
    }

    public String getSeparatorReplacement() {
        return separatorReplacement;
    }

    @Override
    protected Comparator<O> getComparator(int pos) {
        ensureContextIndex(this, pos);
        Context<O> context1 = contexts[pos];
        return context1.getComparator(0);
    }

    protected void ensureContextIndex(MultiJXPathDecorator<?> decorator,
                                      int pos) {
        if (pos < -1 || pos > decorator.contexts.length) {
            throw new ArrayIndexOutOfBoundsException(
                    "context index " + pos +
                    " is out of bound, can be inside [" + 0 + "," +
                    decorator.contexts.length + "]");
        }
    }
}
