/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 *
 * This package contains a api to do some decoration over any object.
 *
 * <h1>Decorator api</h1>
 * The {@link org.nuiton.decorator.Decorator} has an internal state which
 * is the type of object to decorate and a method to do the decoration :
 * {@link org.nuiton.decorator.Decorator#toString(Object)}.
 * <p>
 * There is some default implementations of a decorator using a simple property,
 * or more complex one using jxpath mecanisms.
 *
 * <h1>DecoratorProvider api</h1>
 * The {@link org.nuiton.decorator.DecoratorProvider} api is there to
 * provide some decorators for you,
 *
 * There is a second one {@link org.nuiton.decorator.DecoratorMulti18nProvider}
 * which take account of a given locale (so can be used in web context for
 * example).
 *
 * User has to fill method
 * {@link org.nuiton.decorator.DecoratorProvider}#loadDecorators()}
 * or
 * {@link org.nuiton.decorator.DecoratorMulti18nProvider#loadDecorators(Locale)}
 * to specify which decorators are available.
 *
 * Then it can use the provider using the methods {@code getDecorator(XXX)}
 *
 * <strong>Note:</strong> Decorator can be contextualized so for a same type of
 * object we can provide different way of decorating then.
 *
 * <h1>DecoratorUtil class</h1>
 *
 * This class offers some usefull method to create new decorator via the
 * {@code newXXX} methods and also some method to sort list of objects using a
 * decorator.
 *
 * <strong>Note: </strong> This api comes from the jaxx-runtime project.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 * @see org.nuiton.decorator.Decorator
 * @see org.nuiton.decorator.DecoratorProvider
 * @see org.nuiton.decorator.DecoratorMulti18nProvider
 */
package org.nuiton.decorator;

import java.util.Locale;
