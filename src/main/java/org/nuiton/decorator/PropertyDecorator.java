/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * Simple property decorator based on {@link String#format(String, Object...)}
 * method.
 * <p>
 * To use it, give him a class and the property name to render.
 * <p>
 * For example :
 * <pre>
 * Decorator&lt;Object&gt; d = DecoratorUtil.newPropertyDecorator(PropertyDecorator.class,"property");
 * </pre>
 *
 * @param <O> type of data to decorate
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @see Decorator
 * @since 2.3
 */
public class PropertyDecorator<O> extends Decorator<O> {

    private static final long serialVersionUID = 1L;

    /** Logger */
    static private final Log log = LogFactory.getLog(PropertyDecorator.class);

    /** name of property */
    protected String property;

    protected transient Method m;

    @Override
    public String toString(Object bean) {
        try {
            return getM().invoke(bean) + "";
        } catch (Exception e) {
            log.error("could not convert for reason : " + e, e);
            return "";
        }
    }

    public String getProperty() {
        return property;
    }

    protected PropertyDecorator(Class<O> internalClass,
                                String property) throws NullPointerException {
        super(internalClass);
        if (property == null) {
            throw new NullPointerException("property can not be null.");
        }
        this.property = property;
        // init method
        getM();
    }

    protected Method getM() {
        if (m == null) {
            for (PropertyDescriptor propertyDescriptor : PropertyUtils.getPropertyDescriptors(getType())) {
                if (propertyDescriptor.getName().equals(property)) {
                    m = propertyDescriptor.getReadMethod();
                    break;
                }
            }
            if (m == null) {
                throw new IllegalArgumentException("could not find the property " + property + " in " + getType());
            }
        }
        return m;
    }
}
