/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.jxpath.DynamicPropertyHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A extended handler to deal with map in JXPath contexts.
 * <p>
 * The basic one in JXPath api only deals with map keys as string.
 * <p>
 * We offers a hook in method {@link #getKey(Object)} to obtain a string
 * representation of a real object in map (key or value).
 * <p>
 * More over, you can also access directly to a key or a value, using this
 * syntax :
 * <p>
 * <pre>context.getValue(".[@name='key:programme2']")</pre>
 * <pre>context.getValue(".[@name='value:programme2']")</pre>
 * <p>
 * If the values are iterable, then will scan inot it when looking for a direct
 * value.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @see DynamicPropertyHandler
 * @since 2.3
 */
public class MapPropertyHandler implements DynamicPropertyHandler {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(MapPropertyHandler.class);

    @Override
    public String[] getPropertyNames(Object object) {
        Map<?, ?> map = (Map<?, ?>) object;
        Set<?> set = map.keySet();
        String[] names = new String[set.size()];
        Iterator<?> it = set.iterator();
        for (int i = 0; i < names.length; i++) {
            Object o = it.next();
            names[i] = getKey(o);
        }
        return names;
    }

    @Override
    public Object getProperty(Object object, String propertyName) {
        Map<?, ?> map = (Map<?, ?>) object;
        boolean getKey = false;
        Object property0;
        if (propertyName.startsWith("value:")) {
            propertyName = propertyName.substring(6);
            if (log.isDebugEnabled()) {
                log.debug("property value name " + propertyName);
            }
            property0 = getPropertyValue(map, propertyName);
            if (log.isDebugEnabled()) {
                log.debug("property value = " + property0);
            }
            return property0;
        }
        if (propertyName.startsWith("key:")) {
            propertyName = propertyName.substring(4);
            getKey = true;
        }
        property0 = getPropertyKey(map, propertyName);
        Object result = null;
        if (property0 != null) {
            if (getKey) {
                result = property0;
            } else {
                result = map.get(property0);
            }
        }
        return result;
    }

    @Override
    public void setProperty(Object object, String propertyName, Object value) {
        Map map = (Map) object;
        Object property0 = getPropertyKey(map, propertyName);
        if (property0 != null) {
            map.put(property0, value);
        }
    }

    /**
     * Obtain the key from the map keys which matches the given {@code key}.
     * <p>
     * To compare object ot string, please refers to the method {@link
     * #getKey(Object)}.
     *
     * @param map the map to scan
     * @param key the string representation of the required key as object
     * @return the found key, or {@code null} if not found.
     */
    public Object getPropertyKey(Map<?, ?> map, String key) {
        Set<?> set = map.keySet();
        for (Object o : set) {
            String k = getKey(o);
            if (key.equals(k)) {
                return o;
            }
        }
        return null;
    }

    /**
     * Obtain the value from the map values which matches the given {@code
     * value}.
     * <p>
     * To compare object to string, please refer to the method {@link
     * #getKey(Object)}.
     *
     * @param map   the map to scan
     * @param value the string representation of the value
     * @return the found value, or {@code null} if not found.}
     */
    public Object getPropertyValue(Map<?, ?> map, String value) {
        Collection<?> set = map.values();
        for (Object o : set) {
            if (o instanceof Iterable<?>) {
                Iterable<?> c = (Iterable<?>) o;
                for (Object oo : c) {
                    String k = getKey(oo);
                    if (value.equals(k)) {
                        return oo;
                    }
                }
                continue;
            }
            String k = getKey(o);
            if (value.equals(k)) {
                return o;
            }
        }
        return null;
    }

    /**
     * Obtain a string representation of an object.
     *
     * @param o the object to decorate
     * @return the string representation of the object
     */
    protected String getKey(Object o) {
        String k = String.valueOf(o);
        return k;
    }
}
