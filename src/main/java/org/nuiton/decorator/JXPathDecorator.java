/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JXPath decorator based on {@link String#format(String, Object...)} method.
 * <p>
 * To use it, give to him a expression where all jxpath to apply on bean are
 * boxed in <code>${}</code>.
 * <p>
 * After the jxpath token you must specifiy the formatter to apply of the
 * jxpath token.
 * <p>
 * For example :
 * <pre>
 * Decorator&lt;Object&gt; d = DecoratorUtil.newJXPathDecorator(
 * JXPathDecorator.class,"expr = ${expressions}$s");
 * assert "expr = %1$s" == d.getExpression();
 * assert 1 == d.getNbToken();
 * assert java.util.Arrays.asList("expression") == d.getTokens();
 * assert "expr = %1$s" == d.toString(d);
 * </pre>
 *
 * @param <O> type of data to decorate
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @see Decorator
 * @since 2.3
 */
public class JXPathDecorator<O> extends Decorator<O> {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log = LogFactory.getLog(JXPathDecorator.class);

    /** the computed context of the decorator */
    protected Context<O> context;

    /** nb jxpath tokens to compute */
    protected int nbToken;

    /** the initial expression used to compute the decorator context. */
    protected String initialExpression;

    protected JXPathDecorator(Class<O> internalClass,
                              String expression,
                              Context<O> context)
            throws IllegalArgumentException, NullPointerException {
        super(internalClass);
        initialExpression = expression;
        if (context != null) {
            setContext(context);
        }
    }

    @Override
    public String toString(Object bean) {
        if (bean == null) {
            return null;
        }
        JXPathContext jxcontext = JXPathContext.newContext(bean);
        Object[] args = new Object[nbToken];

        for (int i = 0; i < nbToken; i++) {
            try {
                args[i] = getTokenValue(jxcontext, context.tokens[i]);
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("can not obtain token " + context.tokens[i]
                              + " on object of type " +
                              bean.getClass().getName() +
                              " for reason " + e.getMessage(), e);
                }

            }
        }

        String result;
        try {
            result = String.format(context.expression, args);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Could not format " + context.expression + "" +
                          " with args : " + Arrays.toString(args), eee);
            }
            result = "";
        }
        return result;
    }

    public String getProperty(int pos) {
        return getTokens()[pos];
    }

    public String getExpression() {
        return context.expression;
    }

    public String[] getTokens() {
        return context.tokens;
    }

    public int getNbToken() {
        return nbToken;
    }

    public String getInitialExpression() {
        return initialExpression;
    }

    @Override
    public String toString() {
        return super.toString() + '<' + context + '>';
    }

    public void setContext(Context<O> context) {
        this.context = context;
        nbToken = context.tokens.length;
        // always reset comparator
        //this.context.comparator = null;
        if (log.isDebugEnabled()) {
            log.debug(context);
        }
    }

    @SuppressWarnings({"unchecked"})
    protected Comparator<O> getComparator(int pos) {
        ensureTokenIndex(this, pos);
        return context.getComparator(pos);
    }

    @SuppressWarnings({"unchecked"})
    protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext,
                                                      String token) {
        // assume all values are comparable
        return (Comparable<Comparable<?>>) jxcontext.getValue(token);
    }

    public static class JXPathComparator<O> implements Comparator<O> {

        protected Map<O, Comparable<Comparable<?>>> valueCache;

        private final String expression;

        public JXPathComparator(String expression) {
            this.expression = expression;
            valueCache = new HashMap<O, Comparable<Comparable<?>>>();
        }

        @Override
        public int compare(O o1, O o2) {
            Comparable<Comparable<?>> c1 = valueCache.get(o1);
            Comparable<Comparable<?>> c2 = valueCache.get(o2);
            if (c1 == null) {
                if (c2 == null) {
                    return 0;
                }
                return 1;
            }
            if (c2 == null) {
                return -1;
            }
            return c1.compareTo(c2);
        }

        public void clear() {
            valueCache.clear();
        }

        public void init(JXPathDecorator<O> decorator, List<O> datas) {
            clear();
            for (O data : datas) {
                JXPathContext jxcontext = JXPathContext.newContext(data);
                Comparable<Comparable<?>> key;
                key = decorator.getTokenValue(jxcontext, expression);
                valueCache.put(data, key);
            }
        }
    }

    public static class Context<O> implements Serializable {

        /**
         * expression to format using {@link String#format(String, Object...)},
         * all variables are compute using using the jxpath tokens.
         */
        protected String expression;

        /** list of jxpath tokens to apply on expression */
        protected String[] tokens;

        protected transient Comparator<O> comparator;

        private static final long serialVersionUID = 1L;

        public Context(String expression, String[] tokens) {
            this.expression = expression;
            this.tokens = tokens;
        }

        public String getFirstProperty() {
            return tokens[0];
        }

        public Comparator<O> getComparator(int pos) {
            if (comparator == null) {
                comparator = new JXPathComparator<O>(tokens[pos]);
            }
            return comparator;
        }

        public void setComparator(Comparator<O> comparator) {
            this.comparator = comparator;
        }

        @Override
        public String toString() {
            return "<expression:" + expression + ", tokens:" +
                   Arrays.toString(tokens) + '>';
        }
    }

    protected static void ensureTokenIndex(JXPathDecorator<?> decorator, int pos) {
        if (pos < -1 || pos > decorator.getNbToken()) {
            throw new ArrayIndexOutOfBoundsException(
                    "token index " + pos + " is out of bound, can be inside [" +
                    0 + ',' + decorator.nbToken + ']');
        }
    }
}
