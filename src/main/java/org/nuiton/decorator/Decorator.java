/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import java.io.Serializable;

/**
 * A simple contract to define a String decorator on any java object.
 *
 * @param <O> the type of data to decorate
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public abstract class Decorator<O> implements Serializable {

    private static final long serialVersionUID = -1L;

    /** Type of the data to decorate */
    protected final Class<O> type;

    public Decorator(Class<O> type) throws NullPointerException {
        if (type == null) {
            throw new NullPointerException("type can not be null.");
        }
        this.type = type;
    }

    /**
     * @param bean the bean to decorate
     * @return the string value of the given bean
     */
    public abstract String toString(Object bean);

    public Class<O> getType() {
        return type;
    }
}
