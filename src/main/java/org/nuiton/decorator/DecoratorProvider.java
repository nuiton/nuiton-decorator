/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * A decorator provider.
 * <p>
 * Implements the method {@link #loadDecorators()} to fill the decorators
 * availables.
 * <p>
 * Then can obtain decorator via the methods {@code getDecorator(...)}
 * <p>
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public abstract class DecoratorProvider {

    /** Logger */
    private static final Log log = LogFactory.getLog(DecoratorProvider.class);

    /** Registred decorators. */
    protected List<DecoratorContext<?>> decorators;

    public DecoratorProvider() {
        loadDecorators();
    }

    /** Load all decorators of the provider */
    protected abstract void loadDecorators();

    /**
     * Obtain a decorator for the given object.
     *
     * @param object object of decorated object
     * @param <O>    object of decorated object
     * @return the decorator or {@code null} if not found
     */
    @SuppressWarnings({"unchecked"})
    public <O> Decorator<O> getDecorator(O object) {
        return getDecorator(object, null);
    }

    /**
     * Obtain a decorator given a object and an extra name to qualify the
     * context.
     *
     * @param object object of decorated object
     * @param name   extra name to qualify the decorator to use
     * @param <O>    object of decorated object
     * @return the decorator or {@code null} if not found
     */
    @SuppressWarnings({"unchecked"})
    public <O> Decorator<O> getDecorator(O object, String name) {
        Class<O> k = (Class<O>) object.getClass();
        return getDecoratorByType(k, name);
    }

    /**
     * Obtain a decorator given a type.
     *
     * @param type type of decorated object
     * @param <O>  type of decorated object
     * @return the decorator or {@code null} if not found
     */
    public <O> Decorator<O> getDecoratorByType(Class<O> type) {
        return getDecoratorByType(type, null);
    }

    /**
     * Obtain a decorator given a type and a extra name.
     *
     * @param type type of decorated object
     * @param name extra name to qualify the decorator to use
     * @param <O>  type of decorated object
     * @return the decorator or {@code null} if not found
     */
    public <O> Decorator<O> getDecoratorByType(Class<O> type, String name) {
        DecoratorContext<O> d = getDecoratorContext(type, name);
        return d == null ? null : d.getDecorator();
    }

    public void reload() {
        clear();
        loadDecorators();
    }

    public void registerPropertyDecorator(Class<?> klass,
                                             String expression) {
        registerPropertyDecorator(klass, null, expression);
    }

    public void registerJXPathDecorator(Class<?> klass, String expression) {
        registerJXPathDecorator(klass, null, expression);
    }

    public void registerMultiJXPathDecorator(Class<?> klass,
                                                String expression,
                                                String separator,
                                                String separatorReplacement) {
        registerMultiJXPathDecorator(klass, null, expression, separator,
                                     separatorReplacement);
    }

    public void registerPropertyDecorator(Class<?> klass,
                                             String name,
                                             String expression) {
        Decorator<?> decorator =
                DecoratorUtil.newPropertyDecorator(klass, expression);
        registerDecorator(name, decorator);
    }

    public void registerJXPathDecorator(Class<?> klass,
                                           String name,
                                           String expression) {
        Decorator<?> decorator =
                DecoratorUtil.newJXPathDecorator(klass, expression);
        registerDecorator(name, decorator);
    }

    public void registerMultiJXPathDecorator(Class<?> klass,
                                                String name,
                                                String expression,
                                                String separator,
                                                String separatorReplacement) {
        Decorator<?> decorator = DecoratorUtil.newMultiJXPathDecorator(
                klass, expression, separator, separatorReplacement
        );
        registerDecorator(name, decorator);
    }

    public void registerDecorator(Decorator<?> decorator) {
        registerDecorator(null, decorator);
    }

    /**
     * Register a new decorator in the cache of the provider.
     *
     * @param <T>       type of data decorated
     * @param context   the name decorator
     * @param decorator the decorator to register
     */
    public <T> void registerDecorator(String context,
                                         Decorator<T> decorator) {

        // obtain the decorator context
        DecoratorContext<?> result =
                getDecoratorContext(decorator.getType(), context);

        if (result != null) {
            throw new IllegalArgumentException(
                    "there is an already register decorator with context " +
                    result);
        }

        DecoratorContext<T> decoratorContext =
                new DecoratorContext<T>(context, decorator);
        if (log.isDebugEnabled()) {
            log.debug(decoratorContext);
        }
        getDecorators().add(decoratorContext);
    }

    public void clear() {
        if (decorators != null) {
            decorators.clear();
        }
    }

    protected List<DecoratorContext<?>> getDecorators() {
        if (decorators == null) {
            decorators = new ArrayList<DecoratorContext<?>>();
        }
        return decorators;
    }

    @SuppressWarnings({"unchecked"})
    protected <T> DecoratorContext<T> getDecoratorContext(Class<T> type,
                                                          String context) {
        DecoratorContext<T> result = null;
        if (decorators != null) {
            for (DecoratorContext<?> d : decorators) {
                if (type == null) {
                    if (d.accept(context)) {
                        result = (DecoratorContext<T>) d;
                        break;
                    }
                    continue;
                }
                if (d.accept(type, context)) {
                    result = (DecoratorContext<T>) d;
                    break;
                }
            }
        }
        return result;
    }

    public static class DecoratorContext<T> {

        /** the context name of the decorator */
        final String context;

        /** the decorator */
        final Decorator<T> decorator;

        public DecoratorContext(String context, Decorator<T> decorator) {
            this.context = context;
            this.decorator = decorator;
        }

        public String getContext() {
            return context;
        }

        public Decorator<T> getDecorator() {
            return decorator;
        }

        public Class<T> getType() {
            return decorator.getType();
        }

        public boolean accept(Class<?> type, String context) {
            boolean accept = getType().isAssignableFrom(type) && accept(context);
            return accept;
        }

        public boolean accept(String context) {
            return this.context == null && context == null ||
                   this.context != null && this.context.equals(context);
        }

        @Override
        public String toString() {
            return super.toString() + "<type: " + getType().getName() +
                   ", context :" + context + ">";
        }
    }

}
