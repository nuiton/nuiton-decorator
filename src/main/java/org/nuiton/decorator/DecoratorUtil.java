/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.JXPathDecorator.Context;
import org.nuiton.decorator.JXPathDecorator.JXPathComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Some usefull methods on {@link Decorator} to create, and sort data with
 * decorators.
 * <p>
 * To create a new decorator, use one of the methods : <ul> <li>{@link
 * #newPropertyDecorator(Class, String)}</li> <li>{@link
 * #newJXPathDecorator(Class, String)}</li> <li>
 * {@link #newMultiJXPathDecorator(Class, String, String)})</li>
 * <li>{@link #newMultiJXPathDecorator(Class, String, String, String)})</li>
 * </ul>
 * <p>
 * To sort a list of data, using a {@link JXPathDecorator}, use the method
 * {@link #sort(JXPathDecorator, List, int)}.
 * <p>
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class DecoratorUtil {

    /** Logger */
    private static final Log log = LogFactory.getLog(DecoratorUtil.class);


    /**
     * Factory method to instanciate a new {@link PropertyDecorator} for the
     * given class {@code internlaClass} and a readable property name.
     *
     * @param type     the class of the objects decorated by the new
     *                 decorator
     * @param property the property
     * @param <O>      the generic type of class to be decorated by the new
     *                 decorator
     * @return the new instanciated decorator
     * @throws IllegalArgumentException if the expression is not valid, says:
     *                                  <p>
     *                                  - a missing right brace was detected.
     *                                  <p>
     *                                  - a ${ was found in a jxpath token.
     * @throws NullPointerException     if type parameter is null.
     */
    public static <O> PropertyDecorator<O> newPropertyDecorator(
            Class<O> type,
            String property)
            throws IllegalArgumentException, NullPointerException {
        return new PropertyDecorator<O>(type, property);
    }

    /**
     * Factory method to instanciate a new {@link JXPathDecorator} for the given
     * class {@code internalClass} and expression.
     *
     * @param type       the class of the objects decorated by the new
     *                   decorator
     * @param expression the expression to use to decorated objects
     * @param <O>        the generic type of class to be decorated by the new
     *                   decorator
     * @return the new instanciated decorator
     * @throws IllegalArgumentException if the expression is not valid, says:
     *                                  <p>
     *                                  - a missing right brace was detected.
     *                                  <p>
     *                                  - a ${ was found in a jxpath token.
     * @throws NullPointerException     if type parameter is null.
     */
    public static <O> JXPathDecorator<O> newJXPathDecorator(
            Class<O> type,
            String expression)
            throws IllegalArgumentException, NullPointerException {

        Context<O> context = createJXPathContext(expression);
        return new JXPathDecorator<O>(type, expression, context);
    }

    public static <O> MultiJXPathDecorator<O> newMultiJXPathDecorator(
            Class<O> type,
            String expression,
            String separator)
            throws IllegalArgumentException, NullPointerException {

        MultiJXPathDecorator<O> decorator = newMultiJXPathDecorator(
                type,
                expression,
                separator,
                separator);
        return decorator;
    }

    /**
     * Build a new multi jx path decorator which uise a cycling order while changing the context index.
     * <p>
     * For example, having a initial expression:
     * <pre>A - B - C</pre>
     * <p>
     * When setting conext index to 1, then we will us this expression:
     * <pre>B - C - A</pre>
     *
     * @param type                 type of bean
     * @param expression           initial expression
     * @param separator            expression context separator
     * @param separatorReplacement context separator replacement in decorator
     * @param <O>                  type of bean
     * @return the new decorator
     * @throws IllegalArgumentException ...
     * @throws NullPointerException     ...
     * @since 3.0
     */
    public static <O> MultiJXPathDecorator<O> newMultiJXPathDecorator(
            Class<O> type,
            String expression,
            String separator,
            String separatorReplacement)
            throws IllegalArgumentException, NullPointerException {

        Context<O>[] contexts = createMultiJXPathContext(
                expression,
                separator,
                separatorReplacement);

        return new MultiJXPathDecorator<O>(
                type,
                expression,
                separator,
                separatorReplacement,
                contexts);
    }

    /**
     * Build a new multi jx path decorator which keep order of incoming expression while changing the context index.
     * <p>
     * For example, having a initial expression:
     * <pre>A - B - C</pre>
     * <p>
     * When setting conext index to 1, then we will us this expression:
     * <pre>B - A - C</pre>
     *
     * @param type                 type of bean
     * @param expression           initial expression
     * @param separator            expression context separator
     * @param separatorReplacement context separator replacement in decorator
     * @param <O>                  type of bean
     * @return the new decorator
     * @throws IllegalArgumentException ...
     * @throws NullPointerException     ...
     * @since 3.0
     */
    public static <O> MultiJXPathDecorator<O> newMultiJXPathDecoratorKeepingOrder(
            Class<O> type,
            String expression,
            String separator,
            String separatorReplacement)
            throws IllegalArgumentException, NullPointerException {

        Context<O>[] contexts = createMultiJXPathContextKeepingOrder(
                expression,
                separator,
                separatorReplacement);

        return new MultiJXPathDecorator<O>(
                type,
                expression,
                separator,
                separatorReplacement,
                contexts);
    }

    private static final Object[] EMPTY_CLASS_ARRAY = new Object[0];

    /**
     * Creates a new multijxpath decorator from a given jxpath decorator.
     *
     * If decorator is clonable, just clone it, otherwise creates a new using his definition.
     *
     * @param decorator the decorator to clone
     * @param <O>       generic type of decorator to clone
     * @return cloned decorator
     * @since 3.0
     */
    public static <O> MultiJXPathDecorator<O> cloneDecorator(JXPathDecorator<O> decorator) {
        if (decorator == null) {
            throw new NullPointerException(
                    "can not have a null decorator as parameter");
        }
        String separator;
        String separatorReplacement;

        if (decorator instanceof Cloneable) {
            Cloneable cloneable = (Cloneable) decorator;

            try {
                Object clone = MethodUtils.invokeExactMethod(cloneable,
                                                             "clone",
                                                             EMPTY_CLASS_ARRAY);
                return (MultiJXPathDecorator<O>) clone;
            } catch (Exception e) {
                throw new IllegalStateException("Could not clone decorator " + decorator, e);
            }

        }
        if (decorator instanceof MultiJXPathDecorator<?>) {

            separator = ((MultiJXPathDecorator<?>) decorator).getSeparator();
            separatorReplacement = ((MultiJXPathDecorator<?>) decorator).getSeparatorReplacement();

        } else {

            separator = "??" + new Date().getTime(); // trick to avoid collisions
            separatorReplacement = " - ";
        }

        return newMultiJXPathDecorator(decorator.getType(),
                                       decorator.getInitialExpression(),
                                       separator,
                                       separatorReplacement);

    }

    /**
     * Sort a list of data based on the first token property of a given context
     * in a given decorator.
     *
     * @param <O>       type of data to sort
     * @param decorator the decorator to use to sort
     * @param datas     the list of data to sort
     * @param pos       the index of context to used in decorator to obtain
     *                  sorted property.
     */
    public static <O> void sort(JXPathDecorator<O> decorator,
                                List<O> datas,
                                int pos) {
        sort(decorator, datas, pos, false);
    }

    /**
     * Sort a list of data based on the first token property of a given context
     * in a given decorator.
     *
     * @param <O>       type of data to sort
     * @param decorator the decorator to use to sort
     * @param datas     the list of data to sort
     * @param pos       the index of context to used in decorator to obtain
     *                  sorted property.
     * @param reverse   flag to sort in reverse order if sets to {@code true}
     * @since 2.2
     */
    public static <O> void sort(JXPathDecorator<O> decorator,
                                List<O> datas,
                                int pos,
                                boolean reverse) {
        Comparator<O> c = null;
        boolean cachedComparator = false;
        try {
            c = decorator.getComparator(pos);
            cachedComparator = c instanceof JXPathComparator<?>;

            if (cachedComparator) {
                ((JXPathComparator<O>) c).init(decorator, datas);
            }
            Collections.sort(datas, c);
            if (reverse) {

                // reverse order
                Collections.reverse(datas);
            }
        } finally {
            if (cachedComparator) {
                ((JXPathComparator<?>) c).clear();
            }
        }
    }

    public static <O> Context<O> createJXPathContext(String expression) {
        List<String> lTokens = new ArrayList<String>();
        StringBuilder buffer = new StringBuilder();
        int size = expression.length();
        int end = -1;
        int start;
        while ((start = expression.indexOf("${", end + 1)) > -1) {
            if (start > end + 1) {

                // prefix of next jxpath token
                buffer.append(expression.substring(end + 1, start));
            }

            // seek end of jxpath
            end = expression.indexOf("}", start + 1);
            if (end == -1) {
                throw new IllegalArgumentException(
                        "could not find the rigth brace starting at car " +
                        start + " : " + expression.substring(start + 2));
            }
            String jxpath = expression.substring(start + 2, end);

            // not allowed ${ inside a jxpath token
            if (jxpath.contains("${")) {
                throw new IllegalArgumentException(
                        "could not find a ${ inside a jxpath expression at " +
                        "car " + (start + 2) + " : " + jxpath);
            }

            // save the jxpath token
            lTokens.add(jxpath);

            // replace jxpath token in expresion with a string format variable
            buffer.append('%').append(lTokens.size());
        }
        if (size > end + 1) {

            // suffix after end jxpath (or all expression if no jxpath)
            buffer.append(expression.substring(end + 1));
        }
        String[] tokens = lTokens.toArray(new String[lTokens.size()]);
        return new Context<O>(buffer.toString(), tokens);
    }

    public static <O> Context<O>[] createMultiJXPathContext(
            String expression,
            String separator,
            String separatorReplacement) {
        int sep = expression.indexOf(separator);
        if (sep == -1) {
            Context<O>[] result = newInstance(1);
            result[0] = createJXPathContext(expression);
            return result;
        }

        List<String> tokens = new ArrayList<String>();
        StringTokenizer stk = new StringTokenizer(expression, separator);
        while (stk.hasMoreTokens()) {
            tokens.add(stk.nextToken());
        }

        int nbTokens = tokens.size();
        Context<O>[] contexts = newInstance(nbTokens);
        if (log.isDebugEnabled()) {
            log.debug("Will prepare " + nbTokens + " contexts from [" + expression + "]");
        }
        for (int i = 0; i < nbTokens; i++) {
            StringBuilder buffer = new StringBuilder(expression.length());
            for (int j = 0; j < nbTokens; j++) {
                int index = (i + j) % nbTokens;
                String str = tokens.get(index);

                //replace all '%(index+1)$' pattern with '%(j+1)$'
                Pattern p = Pattern.compile("\\%(" + (index + 1) + ")\\$");
                Matcher matcher = p.matcher(str);
                String safeStr = matcher.replaceAll("\\%" + (j + 1) + "\\$");

                if (log.isDebugEnabled()) {
                    log.debug("[" + (index + 1) + "-->" + (j + 1) + "] " + str +
                              " transformed to " + safeStr);
                }
                buffer.append(separatorReplacement).append(safeStr);
            }
            String expr = buffer.substring(separatorReplacement.length());
            if (log.isDebugEnabled()) {
                log.debug("context [" + i + "] : " + expr);
            }
            contexts[i] = createJXPathContext(
                    expr);
        }
        return contexts;
    }

    public static <O> Context<O>[] createMultiJXPathContextKeepingOrder(
            String expression,
            String separator,
            String separatorReplacement) {
        int sep = expression.indexOf(separator);
        if (sep == -1) {
            Context<O>[] result = newInstance(1);
            result[0] = createJXPathContext(expression);
            return result;
        }

        List<String> tokens = new ArrayList<String>();
        StringTokenizer stk = new StringTokenizer(expression, separator);
        while (stk.hasMoreTokens()) {
            tokens.add(stk.nextToken());
        }

        int nbTokens = tokens.size();
        Context<O>[] contexts = newInstance(nbTokens);
        if (log.isDebugEnabled()) {
            log.debug("Will prepare " + nbTokens + " contexts from [" + expression + "]");
        }
        for (int i = 0; i < nbTokens; i++) {
            StringBuilder buffer = new StringBuilder(expression.length());

            {
                // first set the i context, then keep context in order
                int index = i;
                String str = tokens.get(index);

                //replace all '%(index+1)$' pattern with '%(1)$'
                Pattern p = Pattern.compile("\\%(" + (index + 1) + ")\\$");
                Matcher matcher = p.matcher(str);
                String safeStr = matcher.replaceAll("\\%" + (1) + "\\$");

                if (log.isDebugEnabled()) {
                    log.debug("[" + (index + 1) + "-->" + (1) + "] " + str +
                              " transformed to " + safeStr);
                }
                buffer.append(separatorReplacement).append(safeStr);
            }
            for (int j = 0; j < nbTokens; j++) {
                if (j == i) {

                    // already done
                    continue;
                }

                int index = j;

                String str = tokens.get(index);

                int againstIndex = j < i ? j + 1 : j;

                //replace all '%(index+1)$' pattern with '%(againstIndex+1)$'
                Pattern p = Pattern.compile("\\%(" + (index + 1) + ")\\$");
                Matcher matcher = p.matcher(str);
                String safeStr = matcher.replaceAll("\\%" + (againstIndex + 1) + "\\$");

                if (log.isDebugEnabled()) {
                    log.debug("[" + (index + 1) + "-->" + (againstIndex + 1) + "] " + str +
                              " transformed to " + safeStr);
                }
                buffer.append(separatorReplacement).append(safeStr);
            }
            String expr = buffer.substring(separatorReplacement.length());
            if (log.isDebugEnabled()) {
                log.debug("context [" + i + "] : " + expr);
            }
            contexts[i] = createJXPathContext(
                    expr);
        }
        return contexts;
    }

    @SuppressWarnings("unchecked")
    protected static <O> Context<O>[] newInstance(int size) {
        // fixme how to instanciate a typed array with no checking warning ?
        return new Context[size];
    }
}
