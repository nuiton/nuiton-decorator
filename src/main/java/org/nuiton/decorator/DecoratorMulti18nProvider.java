/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A decorator provider for multi-i18n locale.
 * <p>
 * Implements the method {@link #loadDecorators(Locale)} to fill the decorators
 * availables.
 * <p>
 * Then can obtain decorator via the methods {@code getDecorator(...)}
 * <p>
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public abstract class DecoratorMulti18nProvider {

    /** Logger */
    private static final Log log = LogFactory.getLog(DecoratorProvider.class);

    /**
     * Loaded decorators.
     * <p>
     * This map will be lazy loaded as needed via the method
     * {@link #getDecoratorContexts(Locale, boolean)}.
     */
    protected Map<Locale, Collection<DecoratorContext<?>>> decoratorContexts;


    /**
     * Load all decorators of the provider for the given {@code locale}.
     *
     * @param locale the locale to use to load decorators.
     */
    protected abstract void loadDecorators(Locale locale);


    /**
     * Obtain a decorator for the given object using the given {@code locale}.
     *
     * @param locale user locale
     * @param object object of decorated object
     * @param <O>    object of decorated object
     * @return the decorator or {@code null} if not found
     */
    @SuppressWarnings({"unchecked"})
    public <O> Decorator<O> getDecorator(Locale locale, O object) {
        return getDecorator(locale, object, null);
    }

    /**
     * Obtain a decorator given a object and an extra name to qualify the
     * context using the given {@code locale}.
     *
     * @param locale user locale
     * @param object object of decorated object
     * @param name   extra name to qualify the decorator to use
     * @param <O>    object of decorated object
     * @return the decorator or {@code null} if not found
     */
    @SuppressWarnings({"unchecked"})
    public <O> Decorator<O> getDecorator(Locale locale,
                                         O object,
                                         String name) {
        Class<O> k = (Class<O>) object.getClass();
        return getDecoratorByType(locale, k, name);
    }

    /**
     * Obtain a decorator given a type on the given {@code locale}.
     *
     * @param locale user locale
     * @param type   type of decorated object
     * @param <O>    type of decorated object
     * @return the decorator or {@code null} if not found
     */
    public <O> Decorator<O> getDecoratorByType(Locale locale,
                                               Class<O> type) {
        return getDecoratorByType(locale, type, null);
    }

    /**
     * Obtain a decorator given a type and a extra context name on the given
     * {@code locale}.
     *
     * @param locale user locale
     * @param type   type of decorated object
     * @param name   extra name to qualify the decorator to use
     * @param <O>    type of decorated object
     * @return the decorator or {@code null} if not found
     */
    public <O> Decorator<O> getDecoratorByType(Locale locale,
                                               Class<O> type,
                                               String name) {
        DecoratorContext<O> d = getDecoratorContext(locale, type, name, true);
        return d == null ? null : d.getDecorator();
    }

    public void registerPropertyDecorator(Locale locale,
                                          Class<?> klass,
                                          String expression) {
        registerPropertyDecorator(locale, klass, null, expression);
    }

    public void registerJXPathDecorator(Locale locale,
                                        Class<?> klass, String expression) {
        registerJXPathDecorator(locale, klass, null, expression);
    }

    public void registerMultiJXPathDecorator(Locale locale,
                                             Class<?> klass,
                                             String expression,
                                             String separator,
                                             String separatorReplacement) {
        registerMultiJXPathDecorator(locale, klass, null, expression, separator,
                                     separatorReplacement);
    }

    public void registerPropertyDecorator(Locale locale,
                                          Class<?> klass,
                                          String name,
                                          String expression) {
        Decorator<?> decorator =
                DecoratorUtil.newPropertyDecorator(klass, expression);
        registerDecorator(locale, name, decorator);
    }

    public void registerJXPathDecorator(Locale locale,
                                        Class<?> klass,
                                        String name,
                                        String expression) {
        Decorator<?> decorator =
                DecoratorUtil.newJXPathDecorator(klass, expression);
        registerDecorator(locale, name, decorator);
    }

    public void registerMultiJXPathDecorator(Locale locale,
                                             Class<?> klass,
                                             String name,
                                             String expression,
                                             String separator,
                                             String separatorReplacement) {
        Decorator<?> decorator = DecoratorUtil.newMultiJXPathDecorator(
                klass, expression, separator, separatorReplacement
        );
        registerDecorator(locale, name, decorator);
    }

    public void registerDecorator(Locale locale,
                                  Decorator<?> decorator) {
        registerDecorator(locale, null, decorator);
    }

    /**
     * Register a new decorator in the cache of the provider.
     *
     * @param <T>       type of data decorated
     * @param locale    the given locale to use for this decorator
     * @param context   the name decorator
     * @param decorator the decorator to register
     */
    public <T> void registerDecorator(Locale locale,
                                      String context,
                                      Decorator<T> decorator) {

        // obtain the decorator context
        DecoratorContext<?> result =
                getDecoratorContext(locale,
                                    decorator.getType(),
                                    context,
                                    false
                );

        if (result != null) {
            throw new IllegalArgumentException(
                    "there is an already register decorator with context " +
                    result);
        }

        DecoratorContext<T> decoratorContext =
                new DecoratorContext<T>(context, decorator);
        if (log.isDebugEnabled()) {
            log.debug(decoratorContext);
        }
        getDecoratorContexts(locale, false).add(decoratorContext);
    }

    public void clear() {
        if (decoratorContexts != null) {
            decoratorContexts.clear();
        }
    }

    protected Map<Locale, Collection<DecoratorContext<?>>> getDecoratorContexts() {
        if (decoratorContexts == null) {
            decoratorContexts = new HashMap<Locale, Collection<DecoratorContext<?>>>();
        }
        return decoratorContexts;
    }

    protected Collection<DecoratorContext<?>> getDecoratorContexts(Locale locale,
                                                                   boolean doLoad) {

        Collection<DecoratorContext<?>> decoratorContexts =
                getDecoratorContexts().get(locale);

        if (decoratorContexts == null) {
            decoratorContexts = new ArrayList<DecoratorContext<?>>();
            getDecoratorContexts().put(locale, decoratorContexts);
            if (doLoad) {
                loadDecorators(locale);
            }
        }
        return decoratorContexts;
    }

    @SuppressWarnings({"unchecked"})
    protected <T> DecoratorContext<T> getDecoratorContext(Locale locale,
                                                          Class<T> type,
                                                          String context,
                                                          boolean doLoad) {
        DecoratorContext<T> result = null;

        Collection<DecoratorContext<?>> decoratorContexts =
                getDecoratorContexts(locale, doLoad);

        if (decoratorContexts != null) {
            for (DecoratorContext<?> d : decoratorContexts) {
                if (type == null) {
                    if (d.accept(context)) {
                        result = (DecoratorContext<T>) d;
                        break;
                    }
                    continue;
                }
                if (d.accept(type, context)) {
                    result = (DecoratorContext<T>) d;
                    break;
                }
            }
        }
        return result;
    }

    public static class DecoratorContext<T> {

        /** the context name of the decorator */
        final String context;

        /** the decorator */
        final Decorator<T> decorator;

        public DecoratorContext(String context, Decorator<T> decorator) {
            this.context = context;
            this.decorator = decorator;
        }

        public String getContext() {
            return context;
        }

        public Decorator<T> getDecorator() {
            return decorator;
        }

        public Class<T> getType() {
            return decorator.getType();
        }

        public boolean accept(Class<?> type, String context) {
            boolean accept = getType().isAssignableFrom(type) && accept(context);
            return accept;
        }

        public boolean accept(String context) {
            return this.context == null && context == null ||
                   this.context != null && this.context.equals(context);
        }

        @Override
        public String toString() {
            return super.toString() + "<type: " + getType().getName() +
                   ", context :" + context + ">";
        }
    }
}
