/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class Data {

    int pos;

    String name;

    protected static List<Data> generate(int nb) {
        List<Data> datas = new ArrayList<Data>(nb);
        for (int i = 0; i < nb; i++) {
            datas.add(new Data(i, "name_" + (nb - i)));
        }
        return datas;
    }

    Data(int pos, String name) {
        this.pos = pos;
        this.name = name;
    }

    public int getPos() {
        return pos;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Data{pos=" + pos + ", name=\'" + name + '\'' + '}';
    }
}
