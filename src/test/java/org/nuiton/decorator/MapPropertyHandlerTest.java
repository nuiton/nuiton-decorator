/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.jxpath.JXPathIntrospector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * Tests the class {@link MapPropertyHandler}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class MapPropertyHandlerTest extends JXPathContextTester {


    /** Logger */
    private static final Log log =
            LogFactory.getLog(MapPropertyHandlerTest.class);

    private static final String TWO_ID = "two";

    private static final String ONE_ID = "one";

    @BeforeClass
    public static void beforeClass() {

        // pour initialiser le JXPathContext
        JXPathIntrospector.registerDynamicClass(
                Map.class,
                MapPropertyHandler.class
        );
    }

    @Before
    public void setUp() {

        Map<String, Boolean> map = new TreeMap<String, Boolean>();
        map.put(ONE_ID, true);
        map.put(TWO_ID, false);

        if (log.isInfoEnabled()) {
            log.info("init map : " + map);
        }
        newContext(map);
    }

    @Test
    public void testValues() throws Exception {

        Boolean value;

        value = (Boolean) getValue(".[@name='one']");
        Assert.assertNotNull(value);
        Assert.assertTrue(value);

        value = (Boolean) getValue(".[@name='two']");
        Assert.assertNotNull(value);
        Assert.assertFalse(value);

        value = (Boolean) getValue(".[@name='three']");
        Assert.assertNull(value);
    }

    @Test
    public void testKey() throws Exception {

        String key;

        key = (String) getValue(".[@name='key:one']");
        Assert.assertNotNull(key);
        Assert.assertEquals(ONE_ID, key);

        key = (String) getValue(".[@name='key:two']");
        Assert.assertNotNull(key);
        Assert.assertEquals(TWO_ID, key);

        key = (String) getValue(".[@name='key:fake']");
        Assert.assertNull(key);
    }

    @Test
    public void testValue() throws Exception {

        Boolean value;

        value = (Boolean) getValue(".[@name='value:true']");
        Assert.assertNotNull(value);
        Assert.assertTrue(value);

        value = (Boolean) getValue(".[@name='value:false']");
        Assert.assertNotNull(value);
        Assert.assertFalse(value);

        value = (Boolean) getValue(".[@name='value:fake']");
        Assert.assertNull(value);
    }
}
