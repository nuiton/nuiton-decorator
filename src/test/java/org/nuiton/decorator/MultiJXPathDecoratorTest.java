/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class MultiJXPathDecoratorTest {

    protected MultiJXPathDecorator<?> decorator;

    protected String expected;

    protected String result;

    @After
    public void after() {
        decorator = null;
    }

    @Test(expected = NullPointerException.class)
    public void testNullInternalClass() throws Exception {
        decorator = DecoratorUtil.newMultiJXPathDecorator(null, "hello", "#");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMissingRightBrace() throws Exception {
        decorator = DecoratorUtil.newMultiJXPathDecorator(Object.class, "${haha", "#");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMissingRightBrace2() throws Exception {
        decorator = DecoratorUtil.newMultiJXPathDecorator(Object.class, "${haha${hum}", "#");
    }

    @Test
    public void testNullBean() throws Exception {
        decorator = DecoratorUtil.newMultiJXPathDecorator(Object.class, "hello", "");
        expected = "hello";
        assertEquals(expected, decorator.getExpression());
        assertEquals(0, decorator.nbToken);
        assertEquals(0, decorator.getTokens().length);

        result = decorator.toString(null);
        assertEquals(null, result);
    }

    @Test
    public void testMultiDecorator() throws Exception {

        decorator = DecoratorUtil.newMultiJXPathDecorator(JXPathDecorator.class, "${expression}$s#${nbToken}$d", "#", " - ");
        assertEquals("%1$s - %2$d", decorator.getExpression());
        assertDecoratorInternal();
        assertEquals(2, decorator.contexts.length);
        decorator.setContextIndex(1);
        assertEquals("%1$d - %2$s", decorator.getExpression());
        assertTokens("nbToken", "expression");
        expected = String.format(decorator.getExpression(), decorator.getNbToken(), decorator.getExpression());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator = DecoratorUtil.newMultiJXPathDecorator(JXPathDecorator.class, "${expression}$s ## ${nbToken}$d", " ## ", " - ");
        assertEquals("%1$s - %2$d", decorator.getExpression());
        assertDecoratorInternal();
        assertEquals(2, decorator.contexts.length);
        decorator.setContextIndex(1);
        assertEquals("%1$d - %2$s", decorator.getExpression());
        assertTokens("nbToken", "expression");
        expected = String.format(decorator.getExpression(), decorator.getNbToken(), decorator.getExpression());
        result = decorator.toString(decorator);
        assertEquals(expected, result);
    }

    @Test
    public void testMultiDecorator2() throws Exception {

        decorator = DecoratorUtil.newMultiJXPathDecorator(JXPathDecorator.class, "${expression}$s#${nbToken}$d#${separator}$s", "#", " - ");

        assertEquals("%1$s - %2$d - %3$s", decorator.getExpression());
        assertTokens("expression", "nbToken", "separator");
        assertEquals(3, decorator.contexts.length);

        expected = String.format(decorator.getExpression(), decorator.getExpression(), decorator.getNbToken(), decorator.getSeparator());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator.setContextIndex(1);
        assertEquals("%1$d - %2$s - %3$s", decorator.getExpression());
        assertTokens("nbToken", "separator", "expression");
        expected = String.format(decorator.getExpression(), decorator.getNbToken(), decorator.getSeparator(), decorator.getExpression());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator.setContextIndex(2);
        assertEquals("%1$s - %2$s - %3$d", decorator.getExpression());
        assertTokens("separator", "expression", "nbToken");

        expected = String.format(decorator.getExpression(), decorator.getSeparator(), decorator.getExpression(), decorator.getNbToken());
        result = decorator.toString(decorator);
        assertEquals(expected, result);
    }


    @Test
    public void testDecoratorEspcapeCharacters() throws Exception {

        decorator = DecoratorUtil.newMultiJXPathDecorator(JXPathDecorator.class, "(${expression}$s)#${nbToken}$d", "#", " - ");
        assertEquals("(%1$s) - %2$d", decorator.getExpression());
        assertTokens("expression", "nbToken");
        assertEquals(2, decorator.contexts.length);
        expected = String.format(decorator.getExpression(), decorator.getExpression(), decorator.getNbToken());
        result = decorator.toString(decorator);
        System.out.println("s=" + result);
        assertEquals(expected, result);

        decorator = DecoratorUtil.newMultiJXPathDecorator(JXPathDecorator.class, "${nbToken}$d#(${expression}$s)", "#", " - ");
        assertEquals("%1$d - (%2$s)", decorator.getExpression());
        assertTokens("nbToken", "expression");
        assertEquals(2, decorator.contexts.length);
        expected = String.format(decorator.getExpression(), decorator.getNbToken(), decorator.getExpression());
        result = decorator.toString(decorator);
        System.out.println("s=" + result);
        assertEquals(expected, result);

        DecoratorProvider provider = new MyDummyDecoratorProvider();
        provider.registerMultiJXPathDecorator(MultiJXPathDecorator.class, "(${expression}$s)#${nbToken}$d", "#", " - ");
        decorator = (MultiJXPathDecorator<?>) provider.getDecoratorByType(MultiJXPathDecorator.class);

        assertEquals("(%1$s) - %2$d", decorator.getExpression());
        assertTokens("expression", "nbToken");
        assertEquals(2, decorator.contexts.length);
        expected = String.format(decorator.getExpression(), decorator.getExpression(), decorator.getNbToken());
        result = decorator.toString(decorator);
        System.out.println("s=" + result);
        assertEquals(expected, result);

    }

    @Test
    public void testMultiDecoratorWithMultiRef() throws Exception {

        decorator = DecoratorUtil.newMultiJXPathDecorator(JXPathDecorator.class, "${expression}$s#${nbToken}$d#${separator}$s %3$s", "#", " - ");

        assertEquals("%1$s - %2$d - %3$s %3$s", decorator.getExpression());
        assertTokens("expression", "nbToken", "separator");
        assertEquals(3, decorator.contexts.length);

        expected = String.format(decorator.getExpression(), decorator.getExpression(), decorator.getNbToken(), decorator.getSeparator());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator.setContextIndex(1);
        assertEquals("%1$d - %2$s %2$s - %3$s", decorator.getExpression());
        assertTokens("nbToken", "separator", "expression");
        expected = String.format(decorator.getExpression(), decorator.getNbToken(), decorator.getSeparator(), decorator.getExpression());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator.setContextIndex(2);
        assertEquals("%1$s %1$s - %2$s - %3$d", decorator.getExpression());
        assertTokens("separator", "expression", "nbToken");

        expected = String.format(decorator.getExpression(), decorator.getSeparator(), decorator.getExpression(), decorator.getNbToken());
        result = decorator.toString(decorator);
        assertEquals(expected, result);
    }

    @Test
    public void testMultiDecoratorWithMultiRefKeepingOrder() throws Exception {

        decorator = DecoratorUtil.newMultiJXPathDecoratorKeepingOrder(JXPathDecorator.class, "${expression}$s#${nbToken}$d#${separator}$s %3$s", "#", " - ");

        assertEquals("%1$s - %2$d - %3$s %3$s", decorator.getExpression());
        assertTokens("expression", "nbToken", "separator");
        assertEquals(3, decorator.contexts.length);

        expected = String.format(decorator.getExpression(), decorator.getExpression(), decorator.getNbToken(), decorator.getSeparator());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator.setContextIndex(1);
        assertEquals("%1$d - %2$s - %3$s %3$s", decorator.getExpression());
        assertTokens("nbToken", "expression", "separator");
        expected = String.format(decorator.getExpression(), decorator.getNbToken(), decorator.getExpression(), decorator.getSeparator());
        result = decorator.toString(decorator);
        assertEquals(expected, result);

        decorator.setContextIndex(2);
        assertEquals("%1$s %1$s - %2$s - %3$d", decorator.getExpression());
        assertTokens("separator", "expression", "nbToken");

        expected = String.format(decorator.getExpression(), decorator.getSeparator(), decorator.getExpression(), decorator.getNbToken());
        result = decorator.toString(decorator);
        assertEquals(expected, result);
    }

    @Test
    public void testSort() throws Exception {

        List<Data> datas = Data.generate(10);

        MultiJXPathDecorator<Data> d = DecoratorUtil.newMultiJXPathDecorator(Data.class, "${pos}$d-${name}$s", "-");

        List<Data> sortData = new ArrayList<Data>(datas);
        DecoratorUtil.sort(d, sortData, 0);
        for (int i = 0; i < datas.size(); i++) {
            Data data = datas.get(i);
            Data sData = sortData.get(i);
            assertEquals(data, sData);
        }
        Collections.sort(datas, new Comparator<Data>() {

            @Override
            public int compare(Data o1, Data o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        d.setContextIndex(1);
        DecoratorUtil.sort(d, sortData, 1);
        for (int i = 0; i < datas.size(); i++) {
            Data data = datas.get(i);
            Data sData = sortData.get(i);
            assertEquals(data, sData);
        }
    }

    public void assertDecoratorInternal(String... tokens) {
        assertTokens(tokens);
        expected = String.format(decorator.getExpression(), decorator.getExpression(), decorator.getNbToken());
        result = decorator.toString(decorator);
        assertEquals(expected, result);
    }

    private void assertTokens(String... tokens) {
        if (tokens.length == 0) {
            tokens = new String[]{"expression", "nbToken"};
        }
        assertEquals(tokens.length, decorator.nbToken);
        assertEquals(tokens.length, decorator.getTokens().length);
        for (int i = 0; i < tokens.length; i++) {
            assertEquals(tokens[i], decorator.getTokens()[i]);
        }
    }

    private static class MyDummyDecoratorProvider extends DecoratorProvider {

        @Override
        protected void loadDecorators() {
        }

    }
}
