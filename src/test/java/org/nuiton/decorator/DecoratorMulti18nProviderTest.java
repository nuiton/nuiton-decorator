/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Locale;

/**
 * Tests the {@link DecoratorMulti18nProvider}
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class DecoratorMulti18nProviderTest {

    private static final String BY_NAME = "name";

    private static final String BY_POS = "pos";

    static protected Locale l_fr = Locale.FRANCE;

    static protected Locale l_en = Locale.UK;

    class MyDecoratorProvider extends DecoratorMulti18nProvider {

        @Override
        protected void loadDecorators(Locale locale) {

            registerPropertyDecorator(locale, File.class, "name");
            registerPropertyDecorator(locale, File.class, BY_NAME, "parent");

            registerJXPathDecorator(locale, Class.class, "${simpleName}$s " + locale.getCountry());
            registerJXPathDecorator(locale, Class.class, BY_NAME, "${name}$s " + locale.getCountry());

            registerMultiJXPathDecorator(locale, Data.class, "${name}$s " + locale.getCountry(), "-", " ");
            registerMultiJXPathDecorator(locale, Data.class, BY_POS, "${pos}$d " + locale.getCountry(), "-", " ");
        }
    }

    MyDecoratorProvider provider;

    @Before
    public void beforeTest() throws Exception {
        provider = new MyDecoratorProvider();
    }

    @Test
    public void testGetDecoratorByObject() throws Exception {


        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecorator(l_fr, f);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("myFile", fileDecorator.toString(f));

        Decorator<?> classDecorator;

        classDecorator = provider.getDecorator(l_fr, Class.class);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("File FR", classDecorator.toString(k));

        classDecorator = provider.getDecorator(l_en, Class.class);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("File GB", classDecorator.toString(k));

        Decorator<Data> dataDecorator;

        dataDecorator = provider.getDecorator(l_fr, d);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("name FR", dataDecorator.toString(d));

        dataDecorator = provider.getDecorator(l_en, d);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("name GB", dataDecorator.toString(d));
    }

    @Test
    public void testGetDecoratorByObjectAndName() throws Exception {

        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecorator(l_fr, f, BY_NAME);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("null", fileDecorator.toString(f));

        Decorator<Class> classDecorator;

        classDecorator = provider.getDecoratorByType(l_fr, Class.class, BY_NAME);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("java.io.File FR", classDecorator.toString(k));

        classDecorator = provider.getDecoratorByType(l_en, Class.class, BY_NAME);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("java.io.File GB", classDecorator.toString(k));


        Decorator<Data> dataDecorator;

        dataDecorator = provider.getDecorator(l_fr, d, BY_NAME);
        Assert.assertNull(dataDecorator);

        dataDecorator = provider.getDecorator(l_en, d, BY_NAME);
        Assert.assertNull(dataDecorator);

        dataDecorator = provider.getDecorator(l_fr, d, BY_POS);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("0 FR", dataDecorator.toString(d));

        dataDecorator = provider.getDecorator(l_en, d, BY_POS);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("0 GB", dataDecorator.toString(d));
    }

    @Test
    public void testGetDecoratorByType() throws Exception {

        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecoratorByType(l_fr, File.class);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("myFile", fileDecorator.toString(f));

        Decorator<Class> classDecorator;

        classDecorator = provider.getDecoratorByType(l_fr, Class.class);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("File FR", classDecorator.toString(k));

        classDecorator = provider.getDecoratorByType(l_en, Class.class);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("File GB", classDecorator.toString(k));

        Decorator<Data> dataDecorator;

        dataDecorator = provider.getDecoratorByType(l_fr, Data.class);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("name FR", dataDecorator.toString(d));

        dataDecorator = provider.getDecoratorByType(l_en, Data.class);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("name GB", dataDecorator.toString(d));
    }

    @Test
    public void testGetDecoratorByTypeAndName() throws Exception {
        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecoratorByType(l_fr, File.class, BY_NAME);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("null", fileDecorator.toString(f));

        Decorator<Class> classDecorator;

        classDecorator = provider.getDecoratorByType(l_fr, Class.class, BY_NAME);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("java.io.File FR", classDecorator.toString(k));

        classDecorator = provider.getDecoratorByType(l_en, Class.class, BY_NAME);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("java.io.File GB", classDecorator.toString(k));

        Decorator<Data> dataDecorator;

        dataDecorator = provider.getDecoratorByType(l_fr, Data.class, BY_POS);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("0 FR", dataDecorator.toString(d));

        dataDecorator = provider.getDecoratorByType(l_en, Data.class, BY_POS);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("0 GB", dataDecorator.toString(d));
    }


}
