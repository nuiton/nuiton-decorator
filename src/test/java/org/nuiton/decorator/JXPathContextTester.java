/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.apache.commons.jxpath.JXPathContext;
import org.junit.After;
import org.junit.Assert;

/**
 * A simple class to test JXPath context.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public abstract class JXPathContextTester {

    JXPathContext context;

    @After
    public void after() {
        context = null;

    }

    protected void newContext(Object o) {
        context = JXPathContext.newContext(o);
    }

    protected <T> T getValue(String path) {
        Assert.assertNotNull("pas de context initialisé", context);
        Object value = context.getValue(path);
        return (T) value;
    }

}
