/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class JXPathDecoratorTest {

    protected JXPathDecorator<?> decorator;

    protected String expected;

    protected String result;

    @After
    public void after() {
        decorator = null;
    }

    @Test(expected = NullPointerException.class)
    public void testNullInternalClass() throws Exception {
        decorator = DecoratorUtil.newJXPathDecorator(null, "hello");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMissingRightBrace() throws Exception {
        decorator = DecoratorUtil.newJXPathDecorator(Object.class, "${haha");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMissingRightBrace2() throws Exception {
        decorator = DecoratorUtil.newJXPathDecorator(Object.class, "${haha${hum}");
    }

    @Test
    public void testNullBean() throws Exception {
        decorator = DecoratorUtil.newJXPathDecorator(Object.class, "hello");
        expected = "hello";
        assertEquals(expected, decorator.getExpression());
        assertEquals(0, decorator.nbToken);
        assertEquals(0, decorator.getTokens().length);

        result = decorator.toString(null);
        assertEquals(null, result);
    }

    @Test
    public void testNoJXPath() throws Exception {
        decorator = DecoratorUtil.newJXPathDecorator(Object.class, "hello");
        expected = "hello";
        assertEquals(expected, decorator.getExpression());
        assertEquals(0, decorator.nbToken);
        assertEquals(0, decorator.getTokens().length);

        result = decorator.toString(this);
        assertEquals(expected, result);
    }

    @Test
    public void testDecorator() throws Exception {

        decorator = DecoratorUtil.newJXPathDecorator(
                JXPathDecorator.class, "${expression}$s - ${nbToken}$d");
        assertEquals("%1$s - %2$d", decorator.getExpression());
        assertDecoratorInternal();

        decorator = DecoratorUtil.newJXPathDecorator(
                JXPathDecorator.class, "${expression}${nbToken}");
        assertEquals("%1%2", decorator.getExpression());
        assertDecoratorInternal();

        decorator = DecoratorUtil.newJXPathDecorator(
                JXPathDecorator.class,
                "before ${expression}$s - ${nbToken}$d after");
        assertEquals("before %1$s - %2$d after", decorator.getExpression());
        assertDecoratorInternal();

        decorator = DecoratorUtil.newJXPathDecorator(
                JXPathDecorator.class,
                "before${expression}$s-${nbToken}$dafter");
        assertEquals("before%1$s-%2$dafter", decorator.getExpression());
        assertDecoratorInternal();
    }

    @Test
    public void testDecoratorEspcapeCharacters() throws Exception {

        decorator = DecoratorUtil.newJXPathDecorator(
                JXPathDecorator.class, "(${expression}$s) - ${nbToken}$d");
        assertEquals("(%1$s) - %2$d", decorator.getExpression());
        String s = decorator.toString(decorator);
        assertDecoratorInternal();

    }

    @Test
    public void testSort() throws Exception {

        List<Data> datas = Data.generate(10);

        JXPathDecorator<Data> d = DecoratorUtil.newJXPathDecorator(
                Data.class, "${pos}$d ${name}$s");

        List<Data> sortData = new ArrayList<Data>(datas);
        DecoratorUtil.sort(d, sortData, 0);
        for (int i = 0; i < datas.size(); i++) {
            Data data = datas.get(i);
            Data sData = sortData.get(i);
            assertEquals(data, sData);
        }
        Collections.sort(datas, new Comparator<Data>() {
            @Override
            public int compare(Data o1, Data o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        JXPathDecorator.Context<Data> context = d.context;
        context.setComparator(null);
        DecoratorUtil.sort(d, sortData, 1);
        for (int i = 0; i < datas.size(); i++) {
            Data data = datas.get(i);
            Data sData = sortData.get(i);
            assertEquals(data, sData);
        }
    }


    public void assertDecoratorInternal(String... tokens) {
        assertTokens(tokens);
        expected = String.format(decorator.getExpression(),
                                 decorator.getExpression(),
                                 decorator.getNbToken());
        result = decorator.toString(decorator);
        assertEquals(expected, result);
    }

    private void assertTokens(String... tokens) {
        if (tokens.length == 0) {
            tokens = new String[]{"expression", "nbToken"};
        }
        assertEquals(2, decorator.nbToken);
        assertEquals(2, decorator.getTokens().length);
        assertEquals(tokens[0], decorator.getTokens()[0]);
        assertEquals(tokens[1], decorator.getTokens()[1]);
    }

}
