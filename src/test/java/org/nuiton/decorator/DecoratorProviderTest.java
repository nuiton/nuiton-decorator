/*
 * #%L
 * Nuiton Decorator
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.decorator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

/**
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class DecoratorProviderTest {

    private static final String BY_NAME = "name";

    class MyDecoratorProvider extends DecoratorProvider {

        @Override
        protected void loadDecorators() {

            registerPropertyDecorator(File.class, "name");
            registerPropertyDecorator(File.class, BY_NAME, "parent");

            registerJXPathDecorator(Class.class, "${simpleName}$s");
            registerJXPathDecorator(Class.class, BY_NAME, "${name}$s");

            registerMultiJXPathDecorator(Data.class, "${name}$s", "-", " ");
            registerMultiJXPathDecorator(Data.class, BY_NAME, "${pos}$d", "-", " ");
        }
    }

    DecoratorProvider provider;

    @Before
    public void beforeTest() throws Exception {
        provider = new MyDecoratorProvider();
    }

    @Test
    public void testGetDecoratorByObject() throws Exception {

        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecorator(f);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("myFile", fileDecorator.toString(f));

        Decorator<?> classDecorator = provider.getDecorator(Class.class);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("File", classDecorator.toString(k));

        Decorator<Data> dataDecorator = provider.getDecorator(d);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("name", dataDecorator.toString(d));
    }

    @Test
    public void testGetDecoratorByObjectAndName() throws Exception {

        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecorator(f, BY_NAME);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("null", fileDecorator.toString(f));

        Decorator<Class> classDecorator = provider.getDecoratorByType(Class.class, BY_NAME);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("java.io.File", classDecorator.toString(k));


        Decorator<Data> dataDecorator = provider.getDecorator(d, BY_NAME);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("0", dataDecorator.toString(d));
    }

    @Test
    public void testGetDecoratorByType() throws Exception {

        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecoratorByType(File.class);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("myFile", fileDecorator.toString(f));

        Decorator<Class> classDecorator = provider.getDecoratorByType(Class.class);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("File", classDecorator.toString(k));

        Decorator<Data> dataDecorator = provider.getDecoratorByType(Data.class);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("name", dataDecorator.toString(d));
    }

    @Test
    public void testGetDecoratorByTypeAndName() throws Exception {
        File f = new File("myFile");
        Data d = new Data(0, "name");
        Class<?> k = File.class;

        Decorator<File> fileDecorator = provider.getDecoratorByType(File.class, BY_NAME);
        Assert.assertNotNull(fileDecorator);
        Assert.assertEquals(File.class, fileDecorator.getType());
        Assert.assertEquals("null", fileDecorator.toString(f));

        Decorator<Class> classDecorator = provider.getDecoratorByType(Class.class, BY_NAME);
        Assert.assertNotNull(classDecorator);
        Assert.assertEquals(Class.class, classDecorator.getType());
        Assert.assertEquals("java.io.File", classDecorator.toString(k));


        Decorator<Data> dataDecorator = provider.getDecoratorByType(Data.class, BY_NAME);
        Assert.assertNotNull(dataDecorator);
        Assert.assertEquals(Data.class, dataDecorator.getType());
        Assert.assertEquals("0", dataDecorator.toString(d));
    }


    @Test
    public void testReload() throws Exception {

        int nb = provider.getDecorators().size();
        Assert.assertTrue(nb > 0);

        provider.reload();

        Assert.assertEquals(nb, provider.getDecorators().size());
    }

    @Test
    public void testClear() throws Exception {
        provider.clear();

        Assert.assertTrue(provider.getDecorators().isEmpty());
    }

}
